document.addEventListener , function(){

    const listaPalabras = ['abeja', 'ballena', 'burro', 'gorila' ];
    let adivinarPalabra=[];
    let mostrarPalabra=[];
    let historialLetras=[];
    let numeroIntentos=[7];
    let nodoLetra=document.querySelector('#letra');
    let nodoBoton=document.querySelector('#Boton');
    let nodoResultado=document.querySelector('#resultado');
    let nodoIntentos=document.querySelector('#intentos');
    let nodoHistorial=document.querySelector('#historal');
    
}

function juego(){

    let posAleatoriaListaPalabras= _.random(listaPalabras.length-1);
    let palabraAleatoria=_.random[posAleatoriaListaPalabra];

    adivinarPalabra=palabraAleatoria.split('');

    for(let letra of adivinarPalabra){

        mostrarPalabra.push('_');

    }

    dibujarJuego();
}

function dibujarJuego(){

    nodoResultado.textContent=mostrarPalabra.join(' ');
    nodoIntentos.textContent=numeroIntentos;
    nodoHistorial.textContent=historialLetras.join(' ');
     
}

function comprobraLetra(){

    let letraUsuario=nodoLetra.value;
    nodoLetra.value='';
    nodoLetra.focus();

    for(const[posicion,adivinarLetra] of adivinarPalabra.entries()){

        if(letraUsuario == adivinarLetra){

            mostrarPalabra[posicion]= adivinarLetra;

        }

    }

    if(!adivinarLetra.includes(letraUsuario)){

        numeroIntentos -= 1;

        historialLetras.push(letraUsuario);

    }

    finalizarJuego();

    dibujarJuego();

}

function comprobarEnter(evento){

    if(evento.code == 'Enter'){

        comprobraLetra();

    }
}

function finalizarJuego(){

    if(!mostrarPalabra.includes('_')){

        alert('Felicitaciones.Has ganado!!!');

        location.reload(true);
    }

    if(numeroIntentos == 0){

        alert('Lo sentimos. Has perdido!!! Era: '+adivinarPalabra.join(''));

        location.reload(true);

    }

nodoBoton.addEventListener('click', comprobraLetra);
nodoLetra.addEventListener('Keyup',comprobarEnter);


juego();

}
